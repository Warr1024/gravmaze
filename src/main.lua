love.graphics.setFont(love.graphics.newFont(20))

--[[--------------------------------------------------------------------

PHYSICS KEY:

WALLS:
	Solid where alpha > 127.
	Green component is "inelasticity".

FLOOR:
	Blue is height-map; bluer is higher.
	Green is friction coefficient.
	Death where red > 127.

--------------------------------------------------------------------]]--

local ipairs, type, love = ipairs, type, love
local pi, exp, cos, sin, sqrt = math.pi, math.exp, math.cos, math.sin, math.sqrt

------------------------------------------------------------------------
-- Input handling

-- First 3-axis joystick we detect will be used as the
-- accelerometer input.
local getaccel
for _, j in ipairs(love.joystick.getJoysticks()) do
	local x, y, z, w = j:getAxes()
	if not joy
	and type(x) == "number"
	and type(y) == "number"
	and type(z) == "number"
	and type(w) == "nil" then
		getaccel = function() return j:getAxes() end
	end
end

-- Keyboard controls, for debugging.
if not getaccel then
	local kd = love.keyboard.isDown
	getaccel = function()
		local x = 0
		if kd("left") then x = x - 0.2 end
		if kd("right") then x = x + 0.2 end
		local y = 0
		if kd("up") then y = y - 0.2 end
		if kd("down") then y = y + 0.2 end
		return x, y
	end
end

------------------------------------------------------------------------
-- Load images

-- Physics layers.
local phys_floor = love.graphics.newImage("phys_floor.png")
local phys_ball = love.graphics.newImage("phys_ball.png")

-- Display layers.
local draw_below = love.graphics.newImage("draw_below.png")
local draw_ball = love.graphics.newImage("draw_ball.png")
local draw_above = love.graphics.newImage("draw_above.png")

------------------------------------------------------------------------
-- Calculate some global world stats

-- Gravity constant affects how sensitive accelerometer
-- input is.
local grav = 2000

-- Size of the world that the ball must stay within.
local worldw, worldh = phys_floor:getDimensions()

-- Calculate starting center and radius of the ball,
-- given the ball image, assuming that the input image
-- is a single simple circle.
local startx, starty, radius
do
	local ax, ay, q = 0, 0, 0
	local balldata = phys_ball:getData()
	for x = 0, worldw - 1 do
		for y = 0, worldh - 1 do
			local _, _, _, a = balldata:getPixel(x, y)
			if a > 127 then
				ax = ax + x
				ay = ay + y
				q = q + 1
			end
		end
	end
	startx = ax / q
	starty = ay / q
	radius = sqrt(q / pi)
end

------------------------------------------------------------------------
-- Automatically crop the ball for draw speed.

local offsx, offsy = worldw, worldh
do
	local maxx, maxy = 0, 0
	local data = draw_ball:getData()
	for x = 0, worldw - 1 do
		for y = 0, worldh - 1 do
			local _, _, _, a = data:getPixel(x, y)
			if a > 0 then
				if x < offsx then offsx = x end
				if y < offsy then offsy = y end
				if x > maxx then maxx = x end
				if y > maxy then maxy = y end
			end
		end
	end
	-- 1px border for anti-aliasing purposes.
	offsx = offsx - 1
	offsy = offsy - 1
	maxx = maxx + 1
	maxy = maxy + 1
	local canv = love.graphics.newCanvas(maxx - offsx + 1, maxy - offsy + 1)
	canv:renderTo(function() love.graphics.draw(draw_ball, -offsx, -offsy) end)
	draw_ball = love.graphics.newImage(canv:newImageData())
end
offsx = offsx - startx
offsy = offsy - starty

------------------------------------------------------------------------
-- Helper functions for phys data access

local function mkget(img)
	local data = img:getData()
	local w, h = img:getDimensions()
	local px = data.getPixel
	return function(x, y)
		if x < 0 then x = 0 end
		if x >= w then x = w - 1 end
		if y < 0 then y = 0 end
		if y >= h then y = h - 1 end
		return px(data, x, y)
	end
end
local getfloor = mkget(phys_floor)

------------------------------------------------------------------------
-- Physics calc on update cycle

-- Track ball velocity and position, and reset function.
local x, y, vx, vy
local function reset()
	x = startx
	y = starty
	vx = 0
	vy = 0
end
reset()

-- Perform one iteration of ball physics math.
local function ballphysics(dt)
	-- Ball leaving the world results in death.
	if x < -radius or x > worldw + radius
	or y < -radius or y > worldh + radius then
		return reset()
	end

	-- Check floor immediately under ball center for
	-- local floor effects.
	local die = getfloor(x, y)
	if die > 127 then return reset() end

	-- Initial accel from user input.
	local ax, ay = getaccel()
	ax = ax * grav
	ay = ay * grav

	-- Sample around ball for wall and gradient pressures.
	local normx = 0
	local normy = 0
	local frict = 0
	for theta = 0, pi * 1.98, pi / 20 do
		-- Calculate point on radius.
		local dx = radius * cos(theta)
		local dy = radius * sin(theta)

		-- Add floor gradient rolling.  Note
		-- that walls are simply very steep
		-- floor gradients that the ball rolls
		-- down very quickly.
		r, g, b = getfloor(x + dx, y + dy)
		ax = ax - dx / radius * b * 10
		ay = ay - dy / radius * b * 10
		
		-- Accumulate friction factors.
		frict = frict + g
	end

	-- Apply friction and acceleration.
	frict = exp(-dt * frict * frict / 200000)
	local nvx = vx * frict + ax * dt
	local nvy = vy * frict + ay * dt

	-- Apply velocity and accel to position.
	x = x + ((vx + nvx) * dt + ax * dt * dt) / 2
	y = y + ((vy + nvy) * dt + ay * dt * dt) / 2
	vx = nvx
	vy = nvy
end

-- Love calc update cycle, broken into smaller slices for physics.
local slice = 1 / 120
function love.update(dt)
	while dt > slice do
		ballphysics(slice)
		dt = dt - slice
	end
	if dt > 0 then ballphysics(dt) end
end

------------------------------------------------------------------------
-- Draw world

love.graphics.setFont(love.graphics.newFont(10))
function love.draw()
	love.graphics.setColor(255, 255, 255, 255)

	local dw, dh = love.graphics.getDimensions()
	local scalex = dw / worldw
	local scale = dh / worldh
	if scalex < scale then scale = scalex end
	local tx = (dw - worldw * scale) / 2
	local ty = (dh - worldh * scale) / 2

	love.graphics.push()
	love.graphics.translate(tx, ty)
	love.graphics.scale(scale)

	love.graphics.draw(draw_below, 0, 0)
	love.graphics.draw(draw_ball, x + offsx, y + offsy)
	love.graphics.draw(draw_above, 0, 0)

	love.graphics.pop()

	love.graphics.setColor(0, 0, 0, 64)
	love.graphics.print(love.timer.getFPS(), 1, 1)
	love.graphics.setColor(255, 255, 255, 64)
	love.graphics.print(love.timer.getFPS(), 0, 0)
end

------------------------------------------------------------------------
-- Quit on ESC or back button

function love.keypressed(k) if k == "escape" then love.event.quit() end end